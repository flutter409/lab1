import 'dart:ui';
import 'package:flutter/material.dart';
void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget {
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}
String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";
String koreanGreeting = "annyeong Flutter";
String chineseGreeting = "nihao Flutter";

class _MyStatefulWidgetState extends State<HelloFlutterApp>{
  String displayText = englishGreeting;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,

        home: Scaffold(
          appBar: AppBar(
            title: Text("Hello Flutter"),
            leading: Icon(Icons.home),
            actions: <Widget>[
              IconButton(
                  onPressed: () {
                    setState(() {
                      displayText = displayText == englishGreeting?
                      spanishGreeting : englishGreeting;
                    });
                  },
                  icon: Icon(Icons.thumb_up)),
              IconButton(onPressed: () {
                setState(() {
                  displayText = displayText == englishGreeting?
                  koreanGreeting : englishGreeting;
                });
              },
                  icon: Icon(Icons.feed)),

              IconButton(onPressed: () {
                setState(() {
                  displayText = displayText == englishGreeting?
                  chineseGreeting : englishGreeting;
                });
              },
                  icon: Icon(Icons.favorite_sharp))


            ],
          ),
          body: Center(
            child: Text(
              displayText,
              style: TextStyle(fontSize: 24),
            ),
          ),
        ));
  }
}

// class HelloFlutterApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//         debugShowCheckedModeBanner: false,
//         home: Scaffold(
//           appBar: AppBar(
//             title: Text("Hello Flutter"),
//             leading: Icon(Icons.home),
//             actions: <Widget>[
//               IconButton(onPressed: () {}, icon: Icon(Icons.refresh))
//             ],
//           ),
//           body: Center(
//             child: Text("Hello Flutter", style: TextStyle(fontSize: 24)),
//           ),
//         ));
//   }
// }
